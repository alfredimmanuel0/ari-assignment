### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 9f8d9aa2-962b-4afd-bc4f-e37acc2608e0
begin
	using Markdown
using InteractiveUtils
end

# ╔═╡ ec369254-cca9-11ec-05dd-63e064ba7633
md"# Assignment 1"

# ╔═╡ 83f62d0d-296d-4b4a-85b4-00c81aadbb9e
md"## Problem 1"

# ╔═╡ 4e15f412-3e8a-4ac1-9170-03ce7f03bf24
function responceTime(outboundLatency, processingTime, imboundLatency)
	responceTimeReturn = outboundLatency + processingTime + imboundLatency
	return responceTimeReturn
end

# ╔═╡ 10638e9e-335a-497e-8db5-8b6dfc64e4f3
function latency(RPM)
	latencyReturn = 60000 * RPM
	return latencyReturn
end

# ╔═╡ 84a03091-5941-4339-9749-307e25107ee9
function throughPut(rate, time)
	throughPutReturn = rate * time
	return throughPutReturn
end

# ╔═╡ 6d1655b8-1b62-4f95-87e8-2062bdf6aa73
function successRate(totalSet, successfulSet)
	successRateReturn = (totalSet/successfulSet) * 100
	return successRateReturn
end

# ╔═╡ ab69650f-9ade-445f-8fac-236838d2d39f
begin
	function dialUp(optionSelectNum)
		if optionSelectNum == 1
			#Enter number
			"Enter outbound latency, processing_time and imbound latency in that order"
			# user input 
			outboundLatency = 4
			processingTime =300
			imboundLatency = 400
			responceTime(outboundLatency, processingTime, imboundLatency)
		elseif optionSelectNum == 2
			"Enter number of RPM"
			# user input
			RPM = 6000
			latency(RPM)
		elseif optionSelectNum == 3
			"Enter rate and time in that order to calculate thoughput"
			#user input
			rate = 120
			time = 4000
			throughput(rate, time)
		else optionSelectNum == 4
			"Enter total set and successful sets in that order"
			# user input 
			totalSet = 200
			successfulSet = 100
			successRate(totalSet, successfulSet)
		end
	end
	#"Press 1: for responce time, 2: for latency, 3: for thoughput adn 4: success rate"
	
end
	

# ╔═╡ 100e5d06-ee14-4781-9521-93c5c30c6423


# ╔═╡ 5c1f5825-1e62-4aa0-97a8-c7fae45b5001


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"
"""

# ╔═╡ Cell order:
# ╠═ec369254-cca9-11ec-05dd-63e064ba7633
# ╠═83f62d0d-296d-4b4a-85b4-00c81aadbb9e
# ╠═9f8d9aa2-962b-4afd-bc4f-e37acc2608e0
# ╠═4e15f412-3e8a-4ac1-9170-03ce7f03bf24
# ╠═10638e9e-335a-497e-8db5-8b6dfc64e4f3
# ╠═84a03091-5941-4339-9749-307e25107ee9
# ╠═6d1655b8-1b62-4f95-87e8-2062bdf6aa73
# ╠═ab69650f-9ade-445f-8fac-236838d2d39f
# ╠═100e5d06-ee14-4781-9521-93c5c30c6423
# ╠═5c1f5825-1e62-4aa0-97a8-c7fae45b5001
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
